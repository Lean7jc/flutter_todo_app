import 'dart:async';
import 'package:flutter/material.dart';
import 'package:email_validator/email_validator.dart';

import '/utils/api.dart';

class RegisterScreen extends StatefulWidget {
    @override
    RegisterScreenState createState() => RegisterScreenState();
}

class RegisterScreenState extends State<RegisterScreen> {
    Future<bool>? _futureUser;

    final _formKey = GlobalKey<FormState>();
    final _tffEmailController = TextEditingController();
    final _tffPasswordController = TextEditingController();
    final _tffConfirmPasswordController = TextEditingController();

    void register(BuildContext context){
        setState(() {
        _futureUser = API().register(
            email: _tffEmailController.text, 
            password: _tffEmailController.text
            ).catchError((error){
                showSnackBar(context, error.message);
                });
        });
    }

    void showSnackBar(BuildContext context, String message){
        SnackBar snackBar = new SnackBar(
            content: Text(message),
            duration: Duration(milliseconds: 2000),
        );

        ScaffoldMessenger.of(context).showSnackBar(snackBar);
    }

  @override
  Widget build(BuildContext context) {
    Widget tffEmail = TextFormField(
        decoration: InputDecoration(labelText: 'Email'),
        keyboardType: TextInputType.emailAddress,
        controller: _tffEmailController,
        validator: (email){
            if (email == null || email.isEmpty){
                return 'The Email must be provided';
            }
            else if (EmailValidator.validate(email) == false){
                return 'A valid email must be provided.';
            }
            return null;     
        }   
    );

    Widget tffPassword = TextFormField(
        decoration: InputDecoration(labelText: 'Password'), 
        obscureText: true,
        controller: _tffPasswordController,
        validator: (password) {
            bool isValid = password != null && password.isNotEmpty;
            return (isValid) ? null : 'The password must be provide';
        }
    );

    Widget tffConfirmPassword = TextFormField(
        decoration: InputDecoration(labelText: 'Confirm Password'),
        obscureText: true,
        controller: _tffConfirmPasswordController,
        validator: (passwordConfirmation) {
            if (_tffPasswordController.text != passwordConfirmation){
                return 'The Password confirmation does not match with the password above';
            }else if(passwordConfirmation == null || passwordConfirmation.isEmpty){
                return 'The password confirmation must be provided.';
            }
        }
    );

    Widget btnRegister = Container(
        width: double.infinity,
        margin: EdgeInsets.only(top: 8.0),
        child: ElevatedButton(child: Text('Register'), 
                onPressed: () {
                if (_formKey.currentState!.validate()) {
                    register(context);
                } else {
                    showSnackBar(context, 'Update the registration to pass all validation.');
                }
            }, 
        
        )
    );

    Widget btnGoToLogin = Container(
        width: double.infinity,
        margin: EdgeInsets.only(top: 8.0),
        child: ElevatedButton(child: Text('Go To Login'), 
        onPressed: () {
                Navigator.pop(context);
            }
        )
    );

    Widget formRegister = SingleChildScrollView(
        child:Form(
            key:  _formKey,
            child: Column(
                children: [
                    tffEmail,
                    tffPassword,
                    tffConfirmPassword,
                    btnRegister,
                    btnGoToLogin
                ]
            )
        )   
    );

    Widget registerView = FutureBuilder(
        future: _futureUser,
        builder: (context, snapshot) {
            if (_futureUser == null){
                return formRegister;
            } else if (snapshot.hasData && snapshot.data == true){
                Timer(Duration(seconds: 3), (){
                    Navigator.pushNamedAndRemoveUntil(context, '/', (route) => false);
                });

                return Column(
                    children: [
                        Text('Registration successful. You will be redirected shortly back to the log in page.'),
                        
                    ]
                );
            } else {
                return Center(
                    child:  CircularProgressIndicator()

                );
               
            }
           
        }
        
    );

    return Scaffold(
            appBar: AppBar(title: Text('Todo Account Registration')),
            body: Container(
                width: double.infinity,
                padding: EdgeInsets.all(16.0),
                child: registerView
            )
        );
    }
}
